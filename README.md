Some notes about the tests.    

Steps to run   
clone repository       
npm install
npm run start    
      
1. Technology used: redux for states, functional react, plain js, Material UI. This is a classic CRA implementation, no server side rendering. I approached the  test as if it was a fast POC    
2. I usually do not divide component in businees logic and presentational: this was done for the purpose of going through the code togheter during the interview     
3. No sass/less ( i usually prefer sass ): i used material design components, for theming, so it is CSS-in-JS    
4. The drop-down on the modals update the state with an 'order' state property (like for a shopping cart): please use redux dev tools to test the state    
5. Colors are awful: i concentrate on the code aspect of the test.
6. Thanks!

Please note: i dont have an ipad at hand, so i had to test from chrome emulator ( also, my browser stack license ended last  month and i didnnt renew in time )