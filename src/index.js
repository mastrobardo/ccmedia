import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { applyMiddleware, createStore } from 'redux';
import { createLogger } from 'redux-logger';
// import {requestData} from './redux/action';
import thunkMiddleware from 'redux-thunk';
import App from './App';
import './index.css';
// import store from './store'
import reducers from './redux/reducers';
import { createMuiTheme, responsiveFontSizes} from '@material-ui/core/styles';
import { ThemeProvider } from "@material-ui/styles";
import CssBaseline from '@material-ui/core/CssBaseline';
import {ErrorBoundary} from 'react-error-boundary'

const loggerMiddleware = createLogger();

const store = createStore(
  reducers,
  applyMiddleware(
    thunkMiddleware, 
    // loggerMiddleware
  )
);

let theme = createMuiTheme();
theme = responsiveFontSizes(theme);

ReactDOM.render(
  <ErrorBoundary
    // FallbackComponent={ErrorFallback}
  >
  <ThemeProvider theme={theme}>
    <Provider store={store}>
      <CssBaseline />
      <App />
    </Provider>
  </ThemeProvider>
  </ErrorBoundary>,
  document.getElementById('root')
);
