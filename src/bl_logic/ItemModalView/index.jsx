import React from 'react';
import { connect, useDispatch } from 'react-redux';
import ItemModal from '../../presentational/ItemModal';
import { closeModal } from '../../redux/action';

function ItemModalView(props) {
    const { selected, data, order } = props;
    const dispatch = useDispatch();
    const current = data.filter(ele => ele.id === selected)[0] || {};
    const orderFiltered = !!selected && order && order.length && order.filter(ele => ele.id === selected);
    const currentOrder = orderFiltered[0] !== undefined ? orderFiltered[0].qty : '';
    return (
        <ItemModal open={!!selected} config={current} currentOrder={currentOrder} onClose={() => dispatch(closeModal())}/>
    )
}

const mapStateToProps = (state /* , ownProps */) => ({
    data: state.loadDataReducer.data,
    selected: state.openModalReducer.selectedId,
    order: state.orderReducer.order
  });
  
  export default connect(
    mapStateToProps,
    null,
  )(ItemModalView);