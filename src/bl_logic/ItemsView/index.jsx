import React from 'react';
import { connect } from 'react-redux';
import { makeStyles } from '@material-ui/core/styles';
import GridList from '@material-ui/core/GridList';
import Item from '../../presentational/Item';
import ItemModalView from '../ItemModalView';
import { selectData } from '../../redux/action'


const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
    flexWrap: 'wrap',
    justifyContent: 'space-around',
    overflow: 'hidden',
    backgroundColor: theme.palette.background.paper,
    margin: '0!important',
  },
}));

function ItemsView(props) {
  const { data } = props;
  const classes = useStyles();
  return (
    <div>
      <GridList className={classes.root} cellHeight={180} spacing={50}>
        {
                data.map((ele, idx) => <Item config={ele} key={idx} />)
            }
      </GridList>
      <ItemModalView />
    </div>
  );
}

const mapStateToProps = (state /* , ownProps */) => ({
  data: state.loadDataReducer.data,
});

export default connect(
  mapStateToProps,
  null,
)(ItemsView);
