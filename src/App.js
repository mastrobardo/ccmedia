import React, { useEffect } from 'react';
import './App.css';
import { connect, useDispatch } from 'react-redux';
import { fetchData } from './redux/action';
import Loading from './presentational/Loading';
import ItemsView from './bl_logic/ItemsView';

function App(props) {
  const dispatch = useDispatch();
  useEffect(()=>{
    props.fetchData();
  }, [])

  return (
    <div className="App">
      {
        props.loading ? <Loading /> : <ItemsView />
      }
     
    </div>
  );
}

const mapStateToProps = (state /*, ownProps*/) => {
  return {
    loading: state.loadDataReducer.loading
  }
}

const mapDispatchToProps = { fetchData };

export default connect(
  mapStateToProps,
  mapDispatchToProps
  // null
)(App)
