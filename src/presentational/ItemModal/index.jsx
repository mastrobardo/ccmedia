import React from 'react';
import { withStyles, makeStyles } from '@material-ui/core/styles';
import Dialog from '@material-ui/core/Dialog';
import MuiDialogTitle from '@material-ui/core/DialogTitle';
import MuiDialogContent from '@material-ui/core/DialogContent';
import MuiDialogActions from '@material-ui/core/DialogActions';
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';
import Typography from '@material-ui/core/Typography';
import { useDispatch } from 'react-redux';
import Box from '@material-ui/core/Box';
import ItemIcon from '../ItemIcon';
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';
import InputLabel from '@material-ui/core/InputLabel';
import { order } from '../../redux/action';

const styles = (theme) => ({
  root: {
    margin: 0,
    padding: theme.spacing(2),
  },
  closeButton: {
    position: 'absolute',
    right: theme.spacing(1),
    top: theme.spacing(1),
    color: theme.palette.grey[500],
  },
});

const useStyles = makeStyles({
  description: {
    padding: '0 30px',
    display: 'flex',
    alignItems: 'center',
  },
  selectEmpty: {
    marginTop: 20,
  },
})

const DialogTitle = withStyles(styles)((props) => {
  const { children, classes, onClose, theme, ...other } = props;
  return (
    <MuiDialogTitle disableTypography className={classes.root} {...other}>
      <Typography variant="h4">{children}</Typography>
        <IconButton aria-label="close" className={classes.closeButton} onClick={onClose}>
          <CloseIcon />
        </IconButton>
    </MuiDialogTitle>
  );
});

const DialogContent = withStyles((theme) => ({
    root: {
      padding: theme.spacing(2),
    },
  }))(MuiDialogContent);
  
  const DialogActions = withStyles((theme) => ({
    root: {
      margin: 0,
      padding: theme.spacing(1),
    },
  }))(MuiDialogActions);

function ItemModal(props) {
    const { config, open, onClose, currentOrder } = props;
    const dispatch = useDispatch();
    const classes = useStyles();
    return (
    <div>
      <Dialog aria-labelledby="customized-dialog-title" open={open} fullScreen={false}>
        <DialogTitle id="dialog-title" onClose={onClose}>
         {config && config.name || ''}
        </DialogTitle>
        <DialogContent dividers>
          <Box display="flex" flexDirection="horizontal">
              <Box>
                <ItemIcon image_url={config.image_url} ibu={config.ibu} />
              </Box>
              <Box className={classes.description}>
                <Typography gutterBottom>
                  {config.description}
                </Typography>
              </Box>
          </Box>
         
        </DialogContent>
        <DialogActions>
        <InputLabel id="select-label">Order</InputLabel>
        <Select
        autoWidth={true}
          labelId="select-filled-label"
          id="select-filled"
          value={currentOrder}
          onChange={e => dispatch(order(config.id, e.target.value))}
        >
          <MenuItem value="">
            <em>None</em>
          </MenuItem>
          <MenuItem value={1} name="One Can">One Can</MenuItem>
          <MenuItem value={6} name="One Pack">One Pack</MenuItem>
          <MenuItem value={30} name="Five Pack">Five Pack Discount</MenuItem>
        </Select>
        </DialogActions>
      </Dialog>
      </div>
    ) 
}

export default ItemModal;