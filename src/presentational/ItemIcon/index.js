import React from 'react';
import Box from '@material-ui/core/Box';
import { makeStyles } from '@material-ui/core/styles';
import BackgroundImage from 'react-background-image-loader';

const useStyles = makeStyles((theme) => ({
    wrapper: {
        position: 'relative',
        minWidth: 300,
        minHeight: 300
    },
    rootCircle: {
        position: 'relative',
        width: '100%',
        borderRadius:'50%',
        backgroundColor: 'green',
        overflow: 'hidden',
        border: '9px solid orange'
    },
    image: {
        height: '300px',
        width: 'auto',
        backgroundSize: '40%',
        backgroundRepeat: 'no-repeat',
        backgroundPosition: 'center',
    },
    textBox: {
        backgroundColor: 'orange',
        width: '100%',
        height: 50,
        position: 'absolute',
        bottom: 0,
        color: 'black',
        fontWeight:  'bold',
        textAlign:'center',
        fontSize: 24
    }
}));

const PLACEHOLDER = 'https://via.placeholder.com/150';
function ItemIcon(props) {
    const { image_url, ibu } = props;
    const classes = useStyles();

    return(
        <Box className={classes.wrapper}>
            <Box  className={classes.rootCircle}>
                <BackgroundImage placeholder={PLACEHOLDER} src={image_url || ''} className={classes.image} />
                <Box className={classes.textBox}>
                    IBU:{ibu}
                </Box>
            </Box>
        </Box>
    )
}

export default ItemIcon;