import GridListTile from '@material-ui/core/GridListTile';
import GridListTileBar from '@material-ui/core/GridListTileBar';
import IconButton from '@material-ui/core/IconButton';
import { makeStyles } from '@material-ui/core/styles';
import InfoIcon from '@material-ui/icons/Info';
import React from 'react';
import BackgroundImage from 'react-background-image-loader';
import {useDispatch} from 'react-redux';
import {selectData} from '../../redux/action'

const useStyles = makeStyles((theme) => ({
  root: {
    minWidth: '300px',
    position: 'relative',
    overflow: 'hidden',
    borderRadius: '50px',
    border: '3px solid grey',
    margin: '20px 0'
  },
  icon: {
    color: 'rgba(255, 255, 255, 0.54)',
  },
  image: {
    width: 'auto',
    height: '200px',
    backgroundSize: 'contain',
    backgroundRepeat: 'no-repeat',
    backgroundPosition: 'center',
  },
  ribbon: {
    clipPath:'polygon(100% 2%, 0 0, 100% 100%)',
    backgroundColor: 'red',
    width: 100,
    height: 100
  },
  ribbonContainer: {
    zIndex: 3,
    position: 'absolute',
    right: -5,
    top: -5
  },
  ribbonText: {
    position: 'absolute',
    zIndex: 5,
    marginLeft: -10,
    marginTop: 10
  },
  ibuText: {
    backgroundColor: 'black',
    color: 'white',
    marginTop: 100,
    position: 'absolute',
  }
}));

function Item(props) {
  const { config, key } = props;
  const classes = useStyles();
  const dispatch = useDispatch();
  const openModalAction = () => dispatch(selectData(config.id))
  return (
    <GridListTile className={classes.root} key={key} onClick={openModalAction}>
      <div className={classes.ribbonContainer}>
        <span className={classes.ribbonText}>{config.abv}%</span>
        <div className={classes.ribbon}>
        </div>
      </div>
      <BackgroundImage placeholder={'https://via.placeholder.com/150'} src={config.image_url} className={classes.image} >
        <div className={classes.ibuText}>
          IBU: {config.ibu || 'N/A'}
        </div>
      </BackgroundImage>
      <GridListTileBar
        title={config.name}
        subtitle={<span>{config.tagline}</span>}
        actionIcon={(
          <IconButton aria-label={`info about ${config.name}`} className={classes.icon} onClick={openModalAction} >
            <InfoIcon />
          </IconButton>
              )}
      />
    </GridListTile>
  );
}

export default Item;
