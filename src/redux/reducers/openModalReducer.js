export default function openModalReducer(state = { selectedId: null }, action) {
      switch (action.type) {
        case 'OPEN_MODAL':
          return Object.assign({}, state, { selectedId: action.selectedId } )
          case 'CLOSE_MODAL':
          return Object.assign({}, state, { selectedId: null } )
        default:
          return state
      }
    }