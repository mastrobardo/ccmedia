export default function loadDataReducer(state = { loading: true }, action) {
    switch (action.type) {
      case 'REQUEST_DATA':
        return Object.assign({}, state, { loading: true } )
        case 'RECEIVE_DATA':
        return Object.assign({}, state, { loading: false, data: action.data } )
      default:
        return state
    }
  }