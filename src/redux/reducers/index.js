import { combineReducers } from 'redux';
import loadDataReducer from './loadDataReducer';
import openModalReducer from './openModalReducer';
import orderReducer from './orderReducer';

export default combineReducers({
  loadDataReducer, 
  openModalReducer,
  orderReducer
})