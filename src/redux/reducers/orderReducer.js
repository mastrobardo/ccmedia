export default function openModalReducer(state = { order: [] }, action) {
      switch (action.type) {
        case 'ORDER':
          const filterStateOrder = state.order.filter(ele => ele.id !== action.id);
          return Object.assign({}, state, { order: filterStateOrder.concat([{id: action.id, qty: action.qty}]) } )
        default:
          return state
      }
    }