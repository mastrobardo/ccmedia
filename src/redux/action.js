import fetch from 'cross-fetch';

const API_URL ='https://api.punkapi.com/v2/beers';

export const REQUEST_DATA = 'REQUEST_DATA'
function requestData() {
  return {
    type: REQUEST_DATA,
    loading: true
  }
}

export const RECEIVE_DATA = 'RECEIVE_DATA'
function receiveData(json) {
  return {
    type: RECEIVE_DATA,
    data: json,
    loading: false,
    receivedAt: Date.now()
  }
}

export function fetchData() {
  return function (dispatch) {
    dispatch(requestData())
    return fetch(API_URL)
      .then(
        response => response.json()
      )
      .then(json =>
        dispatch(receiveData(json))
      )
      //np catch we have error boundaries
  }
}

export const OPEN_MODAL = 'OPEN_MODAL'
export function selectData(id) {
  return {
    type: OPEN_MODAL,
    selectedId: id,
  }
}

export const CLOSE_MODAL = 'CLOSE_MODAL'
export function closeModal() {
  return {
    type: CLOSE_MODAL,
    selectedId: null,
  }
}

export const ORDER = 'ORDER'
export function order(id, qty) {
  return {
    type: ORDER,
    id,
    qty
  }
}